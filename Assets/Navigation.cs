using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;


public class Navigation : MonoBehaviour
{
    [SerializeField] List<NavMeshAgent> selectedUnits;
    [SerializeField] List<PositionInfo> targetPosCollection;

    [SerializeField] Camera cam;
    [SerializeField] Image selectionBox;

    [SerializeField] List<NavMeshAgent> selectAbleUnits;

    Vector3 initPos = Vector3.zero;
    Vector3 endPos = Vector3.zero;
    Rect rect = Rect.zero;

    [System.Serializable]
    public struct PositionInfo
    {
        public Vector3 position;
        public float angle;
        public bool check;
        public float angleRange;

        public PositionInfo(Vector3 position, float angle, float angleRange)
        {
            this.position = position;
            this.angle = angle;
            this.angleRange = angleRange;
            check = true;
        }

        public void CheckChange(bool change)
        {
            check = change;
        }

    }

    List<NavMeshAgent> SortShortest(List<NavMeshAgent> entry)
    {
        List<NavMeshAgent> output = new List<NavMeshAgent>();
        List<bool> flagInserted = new List<bool>();
        List<PositionInfo> relativePos;
        float x = 0, z = 0;
        Vector3 center;

        for (int i = 0; i < entry.Count; i++)
        {
            x += entry[i].transform.position.x;
            z += entry[i].transform.position.z;
            flagInserted.Add(true);
        }
        center = new Vector3(x / entry.Count - 1, 0, z / entry.Count - 1);

        relativePos = new List<PositionInfo>(CircularPosition(center, entry.Count));

        for (int i = 0; i < relativePos.Count; i++)
        {
            int index = -1;
            float shortestDistance = Mathf.Infinity;
            float angleRangeStart = relativePos[i].angleRange / 2;
            float angleRangeEnd = relativePos[i].angle + relativePos[i].angleRange / 2f;

            int rotateIndex = 0;
            do
            {
                for (int j = 0; j < entry.Count; j++)
                {
                    float distance = Vector3.Distance(entry[j].transform.position, relativePos[i].position);

                    if (i > 0)
                    {
                        float angle = Vector3.SignedAngle(center, entry[j].transform.position - center, Vector3.down) - 90;
                        if (angle < 0)
                        {
                            angle += 360f;
                        }

                        if (angleRangeEnd >= 360)
                        {
                            if ((angle <= (angleRangeEnd - 360) && angle >= 0) || (angle >= angleRangeStart && angle <= 360))
                            {
                                if (flagInserted[j])
                                {

                                    if (distance < shortestDistance)
                                    {
                                        index = j;
                                        shortestDistance = distance;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if ((angle >= angleRangeStart && angle <= angleRangeEnd))
                            {
                                if (flagInserted[j])
                                {
                                    if (distance < shortestDistance)
                                    {
                                        index = j;
                                        shortestDistance = distance;
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        if (flagInserted[j])
                        {

                            if (distance < shortestDistance)
                            {
                                index = j;
                                shortestDistance = distance;

                            }
                        }
                    }
                }


                if (i + rotateIndex < relativePos.Count - 1)
                {
                    rotateIndex++;
                    angleRangeEnd = relativePos[i + rotateIndex].angle + relativePos[i + rotateIndex].angleRange / 2f;
                }

            } while (index < 0);

            flagInserted[index] = false;
            output.Add(entry[index]);
        }

        return output;
    }



    private void Start()
    {
   ;
    }

    void CreateBox()
    {

        List<NavMeshAgent> tempHolder = new List<NavMeshAgent>();


        if (Input.GetMouseButtonDown(0))
        {
            selectionBox.gameObject.SetActive(true);
            initPos = Input.mousePosition;
        }

        if (selectionBox.gameObject.activeInHierarchy)
        {
            endPos = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {

            Ray ray = Camera.main.ScreenPointToRay(endPos);
            LayerMask layer = LayerMask.GetMask("Unit");

            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layer))
            {
                foreach (NavMeshAgent item in selectAbleUnits)
                {
                    if (hit.transform == item.transform)
                    {
                        tempHolder.Add(item);
                    }
                }
            }
            if (selectionBox.gameObject.activeInHierarchy)
            {
                foreach (NavMeshAgent item in selectAbleUnits)
                {
                    if (rect.Contains(cam.WorldToScreenPoint(item.transform.position)))
                    {
                        tempHolder.Add(item);
                    }
                }
                rect = Rect.zero;
                selectionBox.gameObject.SetActive(false);
            }

            if (tempHolder.Count > 0)
            {
                selectedUnits.Clear();
                selectedUnits = new List<NavMeshAgent>(SortShortest(tempHolder));
            }

        }

        float width = (endPos.x - initPos.x), height = (endPos.y - initPos.y);
        Vector2 boxCenter = new Vector2((initPos.x + endPos.x), (initPos.y + endPos.y)) / 2;
        selectionBox.rectTransform.position = new Vector3(boxCenter.x, boxCenter.y, 0);
        selectionBox.rectTransform.sizeDelta = new Vector2(Mathf.Abs(width), Mathf.Abs(height)) / 2;

        rect = new Rect(boxCenter - selectionBox.rectTransform.sizeDelta, selectionBox.rectTransform.sizeDelta * 2);

    }


    private void OnDrawGizmos()
    {


    }


    void Update()
    {
        CreateBox();

        if (Input.GetMouseButton(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            LayerMask layer = LayerMask.GetMask("Ground");

            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layer))
            {
                targetPosCollection.Clear();
                targetPosCollection = new List<PositionInfo>(CircularPosition(hit.point, selectedUnits.Count));

                for (int i = 0; i < selectedUnits.Count; i++)
                {
                    selectedUnits[i].SetDestination(targetPosCollection[i].position);
                }
            }

        }



    }



    List<PositionInfo> CircularPosition(Vector3 center, int numberOfPos)
    {
        List<PositionInfo> position = new List<PositionInfo>();
        int number = numberOfPos;

        position.Add(new PositionInfo(center, 0, 1)); ;
        for (int i = 1; i < number; i++)
        {
            int whichCircle = 1;
            int index = 0;
            int iTemp = i;
            float distance = 0;
            float angle = 0;

            while (iTemp > 0)
            {

                iTemp -= ((9 * whichCircle) - 3);
                whichCircle++;
            }

            for (int j = 1; j < whichCircle - 1; j++)
            {
                index += (9 * j - 3);
            }
            if (i - index > 0)
            {
                if (number - index < ((9 * whichCircle) - 3))
                {
                    angle = 360f / (float)(number - index - 1);
                }
                else
                {
                    angle = 360f / (float)((9 * (whichCircle - 1)) - 3);
                }

                distance = (whichCircle - 1) * 1.75f;



                position.Add(new PositionInfo
                {
                    position = (new Vector3(distance * Mathf.Cos(angle * ((i - index) % (9 * whichCircle - 3)) * Mathf.Deg2Rad),
                                         0,
                                         distance * Mathf.Sin(angle * ((i - index) % (9 * whichCircle - 3)) * Mathf.Deg2Rad))
                                         + center),
                    angle = angle * ((i - index) % (9 * whichCircle - 3)),
                    angleRange = angle
                });
            }
        }

        return position;
    }


}
