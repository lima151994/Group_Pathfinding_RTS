using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllPermut : MonoBehaviour
{
    public List<int> number = new List<int>();
    public List<string> permutResult = new List<string>();


    private void Awake()
    {

    }

    public List<string> DoPermut(int startIndex, int endIndex)
    {
        number.Clear();
        permutResult.Clear();

        for (int i = startIndex; i <= endIndex; i++)
        {
            number.Add(i);
        }
        
        Permut(number, 0);
        return permutResult;

    }

    void Swap(List<int> theList, int indexA, int indexB)
    {
        int temp = theList[indexA];
        theList[indexA] = theList[indexB];
        theList[indexB] = temp;
    }
    public void Permut(List<int> theList, int f)
    {
        if (f>=theList.Count)
        {
            string result = "";
            for (int i = 0; i < theList.Count; i++)
            {
                result += theList[i].ToString();
            }
            permutResult.Add(result);
            return;
        }

        for (int i = f; i < theList.Count; i++)
        {

            Swap(theList, i, f);
            Permut(theList, f + 1);
            Swap(theList, i, f);

        }

    }


}
